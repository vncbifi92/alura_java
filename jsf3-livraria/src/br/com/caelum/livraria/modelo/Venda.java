package br.com.caelum.livraria.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Venda {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	private Livro livro;
	
	private Integer quantidade;
	
	public Venda() {}
	
	public Venda(Livro livro, Integer quatidade) {
		super();
		this.livro = livro;
		this.quantidade = quatidade;
	}
	
	public Livro getLivro() {
		return livro;
	}
	
	public void setLivro(Livro livro) {
		this.livro = livro;
	}
	
	public Integer getQuatidade() {
		return quantidade;
	}
	
	public void setQuatidade(Integer quatidade) {
		this.quantidade = quatidade;
	}
	
}
