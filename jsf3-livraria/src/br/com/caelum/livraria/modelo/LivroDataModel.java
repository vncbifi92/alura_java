package br.com.caelum.livraria.modelo;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.caelum.livraria.dao.LivroDao;

public class LivroDataModel extends LazyDataModel<Livro> {

	private static final long serialVersionUID = -5406372211973770161L;
	
	@Inject
    LivroDao livroDao;


    @PostConstruct
    public void init() {
        super.setRowCount(this.livroDao.quantidadeDeElementos());
    }
	
	@Override
	public List<Livro> load(int arg0 /*inicio*/, int arg1 /*quantidade*/, String arg2 /*campoOrdenacao*/,
							SortOrder arg3 /*sentidoOrdenacao*/, Map<String, FilterMeta> arg4 /*filtros*/) {
		String titulo  = "";
		String isbn  = "";

		if(arg4.get("titulo") != null) {
			titulo = (String) arg4.get("titulo").getFilterValue();
		}
		
		if(arg4.get("isbn") != null) {
			isbn = (String) arg4.get("isbn").getFilterValue();
		}
		
		if(isbn != null && !isbn.isEmpty()) {
			return livroDao.listaTodosPaginada(arg0, arg1, "isbn", isbn);
			
		}else {
			return livroDao.listaTodosPaginada(arg0, arg1, "titulo", titulo);
		}
	}
	
}
