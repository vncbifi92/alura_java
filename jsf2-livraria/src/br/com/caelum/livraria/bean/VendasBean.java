package br.com.caelum.livraria.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import br.com.caelum.livraria.dao.DAO;
import br.com.caelum.livraria.modelo.Livro;
import br.com.caelum.livraria.modelo.Venda;

@ManagedBean
@ViewScoped
public class VendasBean {

	
	public BarChartModel getVendasModel(){
		
		BarChartModel model = new BarChartModel();

		model.setAnimate(true);
		
		ChartSeries venda = new ChartSeries();
		
		venda.setLabel("Vendas 2021");
		
		for (Venda v : getVendas(1234)) {
			venda.set(v.getLivro().getTitulo(), v.getQuatidade());
		}
		
		ChartSeries venda2 = new ChartSeries();
		
		venda2.setLabel("Vendas 2020");
		
		for (Venda v : getVendas(4321)) {
			venda2.set(v.getLivro().getTitulo(), v.getQuatidade());
		}

		model.addSeries(venda);
		model.addSeries(venda2);
		
		return model;
		
	}
	
	public List<Venda> getVendas(long seed){
		
		List<Livro> livros = new DAO<Livro>(Livro.class).listaTodos();
		List<Venda> vendas = new ArrayList<Venda>();
		
		Random random = new Random(seed);
		for (Livro livro : livros) {
			Integer quantidade = random.nextInt(500);
			
			vendas.add(new Venda(livro, quantidade));
			
		}
		
		return vendas;
		
	}
	
}
