package br.com.alura.jpa.testes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.alura.jpa.modelo.Conta;

public class AlteraSaldoContaVinicius {
	
	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("contas");
		EntityManager em = emf.createEntityManager();
		
		Conta contaDoVinicius = em.find(Conta.class, 3L);
		
		//System.out.println("Conta do Vinicius -> " + contaDoVinicius.getTitular());
		
		em.getTransaction().begin();
		
		contaDoVinicius.setSaldo(20.0);
		
		em.getTransaction().commit();
		
	}

}
