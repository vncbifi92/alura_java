package br.com.alura.jpa.testes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CriaTabelaConta {

	public static void main(String[] args) {

		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("contas");

			@SuppressWarnings("unused")
			EntityManager createEntityManager = emf.createEntityManager();

			emf.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
