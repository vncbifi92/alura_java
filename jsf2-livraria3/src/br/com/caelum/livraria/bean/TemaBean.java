package br.com.caelum.livraria.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class TemaBean {
	private String tema = "vader";

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}
	
	public String[] getTemas() {
		return new String[]{"afterdark","afternoon","afterwork"
							,"black-tie","blitzer","bluesky"
							,"bootstrap","casablanca","cruze"
							,"cupertino","dark-hive","delta"
							,"dot-luv","eggplant","excite-bike"
							,"flick","glass-x","home","hot-sneaks"
							,"humanity","le-frog","luna-amber","luna-blue"
							,"luna-green","luna-pink","midnight","mint-choc"
							,"nova-colored","nova-dark","nova-light","omega"
							,"overcast","pepper-grinder","redmond","rocket"
							,"sam","smoothness","south-street","start"
							,"sunny","swanky-purse","trontastic"
							,"ui-darkness","ui-lightness","vader"};
		
	}
	
}



