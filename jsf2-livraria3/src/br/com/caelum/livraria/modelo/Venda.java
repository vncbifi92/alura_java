package br.com.caelum.livraria.modelo;

public class Venda {
	private Livro livro;
	private Integer quatidade;
	
	public Venda(Livro livro, Integer quatidade) {
		super();
		this.livro = livro;
		this.quatidade = quatidade;
	}
	
	public Livro getLivro() {
		return livro;
	}
	
	public void setLivro(Livro livro) {
		this.livro = livro;
	}
	
	public Integer getQuatidade() {
		return quatidade;
	}
	
	public void setQuatidade(Integer quatidade) {
		this.quatidade = quatidade;
	}
	
}
