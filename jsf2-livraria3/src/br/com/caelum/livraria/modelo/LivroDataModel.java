package br.com.caelum.livraria.modelo;

import java.util.List;
import java.util.Map;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.caelum.livraria.dao.DAO;

public class LivroDataModel extends LazyDataModel<Livro> {

	private static final long serialVersionUID = -5406372211973770161L;
	
	private DAO<Livro> dao = new DAO<Livro>(Livro.class);
	
	public LivroDataModel() {
	    super.setRowCount(dao.quantidadeDeElementos());
	}
	
	@Override
	public List<Livro> load(int arg0 /*inicio*/, int arg1 /*quantidade*/, String arg2 /*campoOrdenacao*/,
							SortOrder arg3 /*sentidoOrdenacao*/, Map<String, FilterMeta> arg4 /*filtros*/) {
		String titulo  = "";
		String isbn  = "";

		if(arg4.get("titulo") != null) {
			titulo = (String) arg4.get("titulo").getFilterValue();
		}
		
		if(arg4.get("isbn") != null) {
			isbn = (String) arg4.get("isbn").getFilterValue();
		}
		
		if(isbn != null && !isbn.isEmpty()) {
			return dao.listaTodosPaginada(arg0, arg1, "isbn", isbn);
			
		}else {
			return dao.listaTodosPaginada(arg0, arg1, "titulo", titulo);
		}
	}
	
}
