package br.com.caelum.livraria.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.caelum.livraria.dao.UsuarioDao;
import br.com.caelum.livraria.modelo.Usuario;
import br.com.caelum.livraria.util.ForwardView;

@Named
@ViewScoped
public class LoginBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private Usuario usuario = new Usuario();

	@Inject
	UsuarioDao dao;
	
	@Inject
	FacesContext context;
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String efetuarLogin() {
		
		boolean existe = dao.existe(this.usuario);
		
		if(existe) {
			context.getExternalContext().getSessionMap().put("usuarioLogado", this.usuario);
			
			return new ForwardView("livro").toString();
		}else {
			
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage("Usuario/senha incorreto"));
			return new ForwardView("login").toString();
		}
	}
	
	public ForwardView deslogar() {
		
		context.getExternalContext().getSessionMap().remove("usuarioLogado");
		
		return new ForwardView("login");
	}
	
}
