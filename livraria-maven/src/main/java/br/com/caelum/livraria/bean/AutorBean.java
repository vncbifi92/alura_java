package br.com.caelum.livraria.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.caelum.livraria.dao.AutorDao;
import br.com.caelum.livraria.modelo.Autor;
import br.com.caelum.livraria.tx.Transacional;
import br.com.caelum.livraria.util.ForwardView;

@Named
@ViewScoped
public class AutorBean  implements Serializable{

	private static final long serialVersionUID = 1L;
	private Autor autor = new Autor();
	
	@Inject
	private AutorDao dao;
	
	public void carregaAutorPelaId() {
		Integer id = this.autor.getId();
		this.autor =this.dao.buscaPorId(id);
		if (this.autor == null) {
			this.autor = new Autor();
	    }
	}

	public Autor getAutor() {
		return autor;
	}
	
	public List<Autor> getAutores() {
		return this.dao.listaTodos();
	}

	@Transacional
	public ForwardView  gravar() {
		System.out.println("Gravando autor " + this.autor.getNome());

		if(this.autor.getId() == null) {
			this.dao.adiciona(this.autor);
		}else {
			this.dao.atualiza(this.autor);
		}
		this.autor = new Autor();
		
		return new ForwardView("livro");
	}
	
	public void carregar(Autor autor) {
		System.out.println("Carregando autor");
		this.autor = autor;
	}
	
	@Transacional
	public void remover(Autor autor) {
		System.out.println("Removendo autor");
		this.dao.remove(autor);
	}
}
